## VMCTRL - QEMU

Tiny (OpenBSD vmctl inspired) virtual machine management script

### CONFIGURATION
export global variables for paths and delay between starting vms

> export VM_PATH="/mnt/four_ab/vms"

> export VM_START_DELAY="5s"

### HOW TO USE

- starting vm
> vmctrl start $name

- stopping vm
> vmctrl kill $name

- installing vm
> vmctrl install $name

- list vms
> vmctrl ls

TODO:
- acpi shutdown signal
- telnet connection for vms running in serial console
- manpage

### LICENSE

PUBLIC DOMAIN
